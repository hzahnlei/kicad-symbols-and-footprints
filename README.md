# KiCad Symbols and Footprints

Many sympols and footprints are availabe for KiCad, either from the original authors or from other sources.
Vintage parts however are sometimes not available or hard to find at least, especially the more obscure ones.
Therefore I created symbols and footprints for some vintage components I needed in my own projecs.
Feel free to download and use them (on your own risk).

2024-02-29, Holger Zahnleiter

## Supported Symbols and Footprints

- An empty field means that the footprint is not applicable to that symbol/part.
- `X` means that the footprint is available.
- `-` means that the footprint is not available but the part is indeed available in the corresponding package.
  This indicates a gap in my library.
  I am mainly modelling easy to work with through-hole packages.

| Symbol  | DIP28 | DIP40 | PLCC44 | DIP48 | FP48 | PLCC48 | PLCC52 | PLCC68 | QUIP64 | Description |
| ------- | :---: | :---: | :----: | :---: | :--: | :----: | :----: | :----: | :----: | ----------- |
| HD6445  |       | X     | -      |       |      |        |        |        |        | CRTC-II, improved cathode ray tube controller |
| IDT7132 |       |       |        | X     | -    | -      | -      |        |        | 2kx8 dual port RAM |
| MCS6502 |       | X     |        |       |      |        |        |        |        | 6502/65C02 MPU | 
| MCS6522 |       | X     |        |       |      |        |        |        |        | 6522/65C22 VIA |
| MCS6551 |       | X     |        |       |      |        |        |        |        | 6551/65C51 ACIA |
| R6501   |       |       |        |       |      |        |        | -      | X      | Microcontroller with 6502 core |
| µPD4464 | X     |       |        |       |      |        |        |        |        | 8kx8 SRAM |

## Installation

Just copy the files contained herein into the respective KiCad folders.
Those folders vary from operating system to operating system.
On macOS, for example, the folder is `~/Documents/KiCad/7.0`.
Be advised that I am using KiCad 7 obviously.
The target folder may change with newer versions of KiCad.

- Copy the folder `footprints/holgers_vintage_footprints.pretty` into the folder `~/Documents/KiCad/7.0/footprints`
- Copy the file `symbols/holgers_vintage_components.kicad_sym` into the folder `~/Documents/KiCad/7.0/symbols`

## Usage

The symbols and footprints can be browsed and picked in the symbols and footprints browsers as usual once they are are installed.

## Acknowledgements

I am thankfully using many free and/or open source libraries, tools, and services.

-  [KiCad](https://www.kicad.org)
-  [GitLab](https://gitlab.com)

## License/Disclaimer

This is a private, free, open source and non-profit project (see LINCENSE file).
Use on your own risk.
I am not liable for any damage caused.
I am a private person, not associated with any companies, or organizations I may have mentioned herein.
